using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerCoin : MonoBehaviour
{
    // Getting The Coin And Cound coin
    public GameManager game;
    public int CoinValue;

    private void OnTriggerEnter2D(Collider2D collision) // How to Get Coin and cound
    {
        game.totalCoin += CoinValue;
        Destroy(gameObject);
    }
}
