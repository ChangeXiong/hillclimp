using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarLogic : MonoBehaviour
{
    private int direction;
    public float gasosa;
    public float gastoGasosa;

    public Image fill;
    public float speed;
    public float carSpeed;
    public float inputMove;
    public Rigidbody2D tireFront;
    public Rigidbody2D tireBack;
    public Rigidbody2D carRb2d;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        fill.fillAmount = gasosa;
    }
    private void FixedUpdate()
    {
        if(gasosa > 0)
        {
            MoveLogic();
            MoveMobile();
        }
        ConsumoLogic();
    }
    void GetInput()
    {
        inputMove = Input.GetAxis("Horizontal");

    }
    void MoveLogic()
    {
        tireFront.AddTorque(-inputMove * speed * Time.fixedDeltaTime);
        tireBack.AddTorque(-inputMove * speed * Time.fixedDeltaTime);
        carRb2d.AddTorque(-inputMove * carSpeed * Time.fixedDeltaTime);
    }
    void MoveMobile()
    {
        tireFront.AddTorque(direction * speed * Time.deltaTime);
        tireBack.AddTorque(direction * speed * Time.deltaTime);
        carRb2d.AddTorque(direction * carSpeed * Time.deltaTime);
    }
    void ConsumoLogic()
    {
        gasosa -= gastoGasosa * Mathf.Abs(inputMove) * Time.fixedDeltaTime;
        gasosa -= gastoGasosa * Mathf.Abs(direction) * Time.deltaTime;
    }
    public void CarInputMobile(int dir)
    {
        direction = dir;
    }
}
