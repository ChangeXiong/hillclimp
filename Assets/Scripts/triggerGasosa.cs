using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerGasosa : MonoBehaviour
{
    public CarLogic car;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        car.gasosa = 1;
        Destroy(gameObject);
    }
}
