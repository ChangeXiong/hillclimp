using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject GameOver;
    public CarLogic car;

    public Text CoinTxt;
    public int totalCoin;


    // How to cound and check km
    public Transform playerPos;
    private Vector2 inicialDist;
    public Text distTxt; // show Km

    private void Start()
    {
        ValueConvert();
        inicialDist = playerPos.position;
    }

    void Update()
    {
        GameOverActive();
        ValueConvert();
        caluloDist();
    }
    void GameOverActive()
    {
        if(car.gasosa <= 0)
        {
            StartCoroutine(TimeGameOverActive());
        }
    }
    IEnumerator TimeGameOverActive()
    {
        yield return new WaitForSeconds(1.0f);
        GameOver.SetActive(true);
    }
    // How wait code Reset game
    public void PlayAgain()
    {
        SceneManager.LoadScene(0);
    }
    public void Quit()
    {
        Application.Quit();
    }
    // Get The Coins and Check count
    void ValueConvert()
    {
        CoinTxt.text = totalCoin.ToString();
    }

    // function Cound kilumaketer
    void caluloDist()
    {
        Vector2 distancia = (Vector2)playerPos.position - inicialDist;
        if(distancia.x < 0)
        {
            distancia.x = 0;
        }
        distTxt.text = distancia.x.ToString("F0") + "M";
    }
}
