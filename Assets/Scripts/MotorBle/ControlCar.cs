using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.StandaloneInputModule;

public class ControlCar : MonoBehaviour
{
    public float MoveSpeed;
    public float CarSpeed;
    public float InputMove;
    public Rigidbody2D tieFornt;
    public Rigidbody2D tieBack;
    public Rigidbody2D CarRbbody;

    public void Update()
    {
        GetInput();
    }
    private void FixedUpdate()
    {
        MoveLogic();
    }
    void GetInput()
    {
        InputMove = Input.GetAxis("Horizontal");
    }
    void MoveLogic()
    {
        tieFornt.AddTorque(-InputMove * MoveSpeed * Time.fixedDeltaTime);
        tieBack.AddTorque(-InputMove * MoveSpeed * Time.fixedDeltaTime);
        CarRbbody.AddTorque(-InputMove * CarSpeed * Time.fixedDeltaTime);
    }
  /*  void MoveMobile()
    {
        tieFornt.AddTorque(direction * speed * Time.deltaTime);
        tieBack.AddTorque(direction * speed * Time.deltaTime);
        CarRbbody.AddTorque(direction * carSpeed * Time.deltaTime);
    }*/
}
